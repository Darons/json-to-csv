const { Parser } = require("json2csv");
const fs = require("fs");
const axios = require("axios");
const convertCsvToXlsx = require("@aternus/csv-to-xlsx");

async function login(user, pass) {
  try {
    const options = {
      headers: {
        Accept: "application/json",
        "Content-Type": "*/*",
      },
    };
    const data = {
      usuario: user,
      clave: pass,
    };
    const response = await axios.post(
      "http://www.cobeca.com:8080/pedidofl/api/login",
      data,
      options
    );
    return response.data.token;
  } catch (e) {
    console.log(e);
  }
}
async function getProducts(token) {
  try {
    const options = {
      headers: {
        Accept: "application/json",
        "Content-Type": "*/*",
        "User-Agent": "User-Agent52",
        Autorizacion: `Bearer ${token}`,
      },
    };
    const data = {
      cod_drogueria: 13,
    };

    const response = await axios.post(
      "http://www.cobeca.com:8080/pedidofl/api/articulos",
      data,
      options
    );
    let obj = response.data.articulos;
    return [
      ...new Map(obj.map((item) => [item["cod_articulo"], item])).values(),
    ];
  } catch (e) {
    console.log(e);
  }
}

async function writeCSV() {
  try {
    const fields = [
      {
        label: "Código barra",
        value: "cod_barra",
      },
      {
        label: "Prov",
        value: "desc_proveedor",
      },
      {
        label: "Cod Prod",
        value: "cod_articulo",
      },
      {
        label: "Descripción del Artículo ",
        value: "desc_articulo",
      },
      {
        label: "Precio",
        value: "monto_final",
      },
      {
        label: "EXISTENCIA",
        value: "existencia",
      },
      {
        label: "OFERTA",
        value: "porcentaje_descuento",
      },
    ];
    const time = new Date().getTime();
    fs.writeFile(
      `./${time}articulos.csv`,
      new Parser({ fields }).parse(unique),
      "utf8",
      (err) => {
        if (err) throw err;
        else console.log("File saved");
      }
    );
    fs.writeFile(
      `./${time}articulosFull.csv`,
      new Parser().parse(unique),
      "utf8",
      (err) => {
        if (err) throw err;
        else console.log("File saved");
      }
    );

    convertCsvToXlsx(`${time}articulos.csv`, `${time}articulos.xlsx`);
    convertCsvToXlsx(`${time}articulosFull.csv`, `${time}articulosFull.xlsx`);
  } catch (err) {
    console.error(err);
  }
}

writeCSV();
